# Go-Sec: Security Packages for Go
[![tests](https://gitlab.com/mbecker/go-sec/workflows/tests/badge.svg)](https://gitlab.com/mbecker/go-sec/actions?query=workflow%3Atests)

This repository includes security packages for LinkSmart Go services.

## Import
```
go get gitlab.com/mbecker/go-sec
```

It includes the following packages:
### Auth
[![GoDoc](https://godoc.org/gitlab.com/mbecker/go-sec/auth?status.svg)](https://godoc.org/gitlab.com/mbecker/go-sec/auth)  
Auth consists of the following subpackages:
* `gitlab.com/mbecker/go-sec/auth/obtainer` interface to obtain OpenID Connect tokens
* `gitlab.com/mbecker/go-sec/auth/validator` interface to validate OpenID Connect tokens
* `gitlab.com/mbecker/go-sec/auth/keycloak` with two packages implementating obtainer and validator for Keycloak

Documentation:
* [Authentication](https://gitlab.com/mbecker/go-sec/wiki/Authentication)

### Authz
[![GoDoc](https://godoc.org/gitlab.com/mbecker/go-sec/authz?status.svg)](https://godoc.org/gitlab.com/mbecker/go-sec/authz)  
Package `gitlab.com/mbecker/go-sec/authz` is a simple rule-based authorization that can be used to implement access control in services after authentication.


Documentation:
* [Authorization](https://gitlab.com/mbecker/go-sec/wiki/Authorization)

## Development
The dependencies of this package are managed by [Go Modules](https://blog.golang.org/using-go-modules).
